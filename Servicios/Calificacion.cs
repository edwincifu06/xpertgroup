﻿using System;

namespace Servicios
{
    public class Calificacion
    {
        string conversacion;
        string palabraCalfAlta = "EXCELENTE SERVICIO";
        string[] palabarasEvaluar = { "Gracias", "Buena Atención", "Muchas Gracias" };
        int puntajeTotal = 0;
        public Calificacion(string Cconversacion)
        {
            this.conversacion = Cconversacion;
        }
        public string calificarConversacion()
        {
            try
            {

                if (this.conversacion != " ")
                {
                    // Mostrar el mensaje especifico en la consola
                    Console.Write("CONVERSACIÓN: \n" + this.conversacion);

                    // Calificar Mensajes
                    puntajeTotal = puntajeTotal + this.calificarNumMensajes(conversacion);

                    Console.Write("PUNTAJE TOTAL MENSAJES " + this.puntajeTotal);

                    if (this.puntajeTotal > 0)
                    {
                        // Calificar coincidencias palabra 'URGENTE'
                        this.puntajeTotal = this.puntajeTotal + this.calificarPalUrgente(conversacion);

                        Console.Write("PUNTAJE TOTAL PALABRA URGENTE " + this.puntajeTotal);

                        if (this.puntajeTotal > 0 || this.puntajeTotal == 0)
                        {

                            // Calificar coincidencias palabra 'EXCELENTE SERVICIO'
                            this.puntajeTotal = this.puntajeTotal + this.calificarCoincidenciasPlbs(this.palabraCalfAlta);

                            Console.Write("PUNTAJE TOTAL PALABRA EXCELENTE SERVICIO " + this.puntajeTotal);

                            if (this.puntajeTotal < 100)
                            {
                                for (int i = 0; i < this.palabarasEvaluar.Length; i++)
                                {
                                    this.puntajeTotal = this.puntajeTotal + this.calificarCoincidenciasPlbs(this.palabarasEvaluar[i]);

                                    Console.Write("PUNTAJE TOTAL PALABRA " + this.palabarasEvaluar[i] + " - " + this.puntajeTotal);
                                }

                                if (this.puntajeTotal > 0 || this.puntajeTotal == 0)
                                {
                                    if (this.puntajeTotal > 0 || this.puntajeTotal == 0)
                                    {
                                        // Calificar duración de la conversación
                                        this.puntajeTotal = this.puntajeTotal + this.calificarTiempoDuracion();
                                    }
                                }
                            }

                        }
                    }

                    return "Exito";

                }
                else
                {
                    return "Error";
                }
            }
            catch (Exception e)
            {

                return "Exception";
            }
        }

        // Calificar Numero Mensajes
        public int calificarNumMensajes(string conversacion)
        {
            int puntos = 0;

            // Indicar cuantos mensaje hay
            Console.Write("NUMERO DE MENSAJES: \n" + this.conversacion.Length);

            if (this.conversacion.Length == 1)
            {
                puntos = -100;
            }
            else if (this.conversacion.Length <= 5)
            {
                puntos = 20;
            }
            else
            {
                puntos = 10;
            }

            return puntos;
        }

        // Función - Calcula número de estrellas
        public string calacularNumEstrellas(int puntaje)
        {

            string numEstrellas = "";

            if (puntaje == 0)
            {
                numEstrellas = "";
            }
            else if (puntaje > 0 && puntaje <= 25)
            {
                numEstrellas = "*";
            }
            else if (puntaje > 25 && puntaje <= 50)
            {
                numEstrellas = "**";
            }
            else if (puntaje > 50 && puntaje <= 75)
            {
                numEstrellas = "***";
            }
            else if (puntaje > 75 && puntaje <= 90)
            {
                numEstrellas = "****";
            }
            else if (puntaje > 90)
            {
                numEstrellas = "*****";
            }

            return numEstrellas;
        }

        // Función - Califica Coincidencias Palabra 'URGENTE'
        public int calificarPalUrgente(string conversacion)
        {
            int puntos = 0;
            int conteo = 0;
            string[] palabara = { "URGENTE" };
            for (int i = 0; i < this.conversacion.Length; i++)
            {

                conversacion = this.conversacion.Replace("/[.,] / g", "");
                Console.Write(this.conversacion[i]);

                string linea = conversacion;

                for (int j = 0; j < linea.Length; j++)
                {
                    Console.Write(linea);
                    if (linea[j].Equals( palabara[0]))
                    {
                        conteo++;
                    }
                }
            }

            Console.Write(conteo);

            if (conteo <= 2)
            {
                puntos = -5;
            }
            else if (conteo > 2)
            {
                puntos = -10;
            }
            else
            {
                puntos = 0;
            }

            return puntos;
        }

        // Función - Califica Coincidencias Palabras definidas
        public int calificarCoincidenciasPlbs(string palabra)
        {

            int puntos = 0;

            for (int i = 0; i < this.conversacion.Length; i++)
            {

                Console.Write(this.conversacion[i]);

                Console.Write(palabra);
                
                int result = conversacion.IndexOf(palabra);
                Console.Write(result);

                if (result != -1)
                {
                    if (palabra == this.palabraCalfAlta)
                    {
                        puntos = 100;
                        break;
                    }
                    else
                    {
                        puntos = 10;
                    }
                }
            }

            return puntos;
        }

        // Función - Califica Tiempo Duración
        public int calificarTiempoDuracion()
        {
            int puntos = 0;

            for (int j = 0; j < this.conversacion.Length; j++)
            {
                Console.Write(this.conversacion[j]);

                string horaInicio = this.conversacion.Substring(0, 8);
                string horaFinal = "";//this.conversacion[this.conversacion.Length - 1].Substring(0, 8);

                Console.Write(horaInicio);
                Console.Write(horaFinal);

                string hora1 = "";//new DateTime("", "", "", horaInicio[0], horaInicio[1], horaInicio[2]);

                string hora2 = "";//new DateTime("", "", "", horaFinal[0], horaFinal[1], horaFinal[2]);

                // Diferencia de horas en segundos
                int dif = 0;//(hora2 - hora1) / 1000;

                Console.Write(hora1);
                Console.Write(hora2);
                Console.Write(dif);

                if (dif < 60)
                {
                    puntos = 50;
                }
                else
                {
                    puntos = 25;
                }

                
            }
            return puntos;
        }
    }
}
